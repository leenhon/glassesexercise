import React, { Component } from "react";
import GlassesList from "./GlassesList";

export default class ChooseGlassExcerise extends Component {
  

  state = {
    glassCur: {
      id: 1,
      price: 30,
      name: "GUCCI G8850U",
      url: "./glassesImage/v1.png",
      desc: "Light pink square lenses define these sunglasses, ending with amother of pearl effect tip. ",
    },
  };

  handleChangeGlassIndex = (newGlass) => {
    this.setState( {
        glassCur: newGlass,
    })
  };


  render() {
    const styleGlass = {
      width: "150px",
      top: "76px",
      right: "117px",
      opacity: "0.7",
    };
    const styleDesGlass = {
      width: "250px",
      top: "200px",
      left: "318px",
      backgroundColor: "rgba(255,127,0,.5)",
      textAlign: "left",
      height: "105px",
    };
    return (
      <div
        style={{
          backgroundImage: "url(./glassesImage/background.jpg)",
          backgroundSize: "2000px",
          minHeight: "900px",
        }}
      >
        <div style={{ backgroundColor: "rgba(0,0,0,.7)", minHeight: "900px" }}>
          <h3
            style={{ backgroundColor: "rgba(0,0,0,.3)" }}
            className="text-center text-light p-5"
          >
            TRY GLASS APP ONLINE
          </h3>
          <div className="container">
            <div className="row mt-5 text-center">
              <div className="col-6">
                <div className="position-relative">
                  <img
                    className="position-absolute"
                    style={{ width: "250px" }}
                    src={"./glassesImage/model.jpg"}
                    alt="model"
                  />
                  <img
                    style={styleGlass}
                    src={this.state.glassCur.url}
                    className="position-absolute"
                  />
                  <div className="position-relative" style={styleDesGlass}>
                    <span
                      style={{
                        color: "#3c21a7",
                        fontSize: "18px",
                        fontWeight: "bold",
                      }}
                      className="font-weight-bold"
                    >
                      Tên kính:{this.state.glassCur.name}
                    </span>
                    <br></br>
                    <p style={{ fontSize: "13px", fontWeight: "400px" }}>
                      Mô tả: {this.state.glassCur.desc}
                    </p>
                  </div>
                </div>
              </div>
              <div className="col-6">
                <img
                  style={{ width: "250px" }}
                  src="./glassesImage/model.jpg"
                  alt="model"
                />
              </div>
            </div>
          </div>

          {/* List Glass */}
          <GlassesList handleChangeGlassIndex={this.handleChangeGlassIndex}></GlassesList>
        </div>
      </div>
    );
  }
}
