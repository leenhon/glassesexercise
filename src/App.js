import logo from "./logo.svg";
import "./App.css";
import Header from "./components/Header";
import Navigation from "./components/Navigation";
import ExampleLayout from "./components/ExampleLayout";
import LearningDataBinding from "./components/LearningDataBinding";
import ExampleShowRoom from "./components/ExampleShowRoom";
import ChooseGlassExcerise from "./components/ChooseGlassExercise";

// JSX : html + js
function App() {
  return (
    <div>
      {/* <LearningDataBinding></LearningDataBinding>
      <ExampleLayout></ExampleLayout>
      <ExampleShowRoom /> */}
      <ChooseGlassExcerise></ChooseGlassExcerise>
    </div>
  );
}

export default App;
